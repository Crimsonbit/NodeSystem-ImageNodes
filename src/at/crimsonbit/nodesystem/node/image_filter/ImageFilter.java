package at.crimsonbit.nodesystem.node.image_filter;

import at.crimsonbit.nodesystem.gui.node.GNode;
import at.crimsonbit.nodesystem.node.IGuiNodeType;
import javafx.scene.paint.Color;

public enum ImageFilter implements IGuiNodeType {

	IMAGE_ADD("Add-Filter Node"), IMAGE_SUBTRACT("Subtract-Filer Node"), IMAGE_MULTIPLY(
			"Multiply-Filter Node"), IMAGE_NEGATE("Negate-Filter Node"), IMAGE_DIVIDE(
					"Divide-Filter Node"), IMAGE_SOBEL(
							"Sobel-Filter Node"), IMAGE_SMOOTHEN("Smoothen-Filter Node"), IMAGE_NORMALIZE(
									"Normalize-Filter Node"), IMAGE_BLUR("Blur-Filter Node"), IMAGE_GRAYSCALE(
											"Grayscale-Filter Node"), IMAGE_RESIZE("Resize-Filter Node");

	private String name;

	private ImageFilter(String s) {

		this.name = s;
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public Class<? extends GNode> getCustomNodeClass() {
		return GNode.class;
	}

	@Override
	public Color getColor() {
		return Color.DARKGREEN;
	}

}
