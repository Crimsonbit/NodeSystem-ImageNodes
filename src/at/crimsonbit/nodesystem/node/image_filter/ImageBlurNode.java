package at.crimsonbit.nodesystem.node.image_filter;

import java.awt.image.BufferedImage;

import at.crimsonbit.nodesystem.nodebackend.api.AbstractNode;
import at.crimsonbit.nodesystem.nodebackend.api.NodeField;
import at.crimsonbit.nodesystem.nodebackend.api.NodeInput;
import at.crimsonbit.nodesystem.nodebackend.api.NodeOutput;
import at.crimsonbit.nodesystem.nodebackend.api.NodeType;
import at.crimsonbit.nodesystem.util.ImageUtils;

public class ImageBlurNode extends AbstractNode {

	@NodeType
	private static final ImageFilter type = ImageFilter.IMAGE_BLUR;

	@NodeInput
	BufferedImage image;

	@NodeInput
	int radius;

	@NodeOutput("computeBlur")
	BufferedImage output;

	public ImageBlurNode() {

	}

	public void computeBlur() {
		if (image != null && radius != 0)
			output = ImageUtils.blur2(image, radius);
	}

}
